package com.example.springboottutorial.service;

import com.example.springboottutorial.StudentRepository;
import com.example.springboottutorial.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public List getStudentName(){
        return List.of(new Student(
                        20, "Juan Dela Cruz", LocalDate.of(1992,8,20)),
                new Student(
                        20, "John Doe", LocalDate.of(1982,8,20)));
    }

    public List<Student> getStudentRecord(){
        return studentRepository.findAll();
    }

    public void addNewStudent(Student student){
        //avoids overriding of records when same id
        Optional<Student> studentId = studentRepository.findById(student.getId());
        if(studentId.isPresent()){
            throw new IllegalStateException("ID already taken.");
        }
        studentRepository.save(student);
    }

    public void removeStudent(int id){
        studentRepository.deleteById(id);
    }

    //Using the save functionality when id exist
    public void updateStudent(int id, String name, LocalDate dob){
        Optional<Student> student = studentRepository.findById(id);

        if(student.isPresent()){
            Student updatedStudent = new Student(id,name,dob);
            studentRepository.save(updatedStudent);
        }else{
            throw new IllegalStateException("ID not exist!");
        }

    }
}
