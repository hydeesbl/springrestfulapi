package com.example.springboottutorial.controller;

import com.example.springboottutorial.model.Student;
import com.example.springboottutorial.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(path = "/name")
    public List getName1(){
        return List.of("Hello","again");
    }
    @GetMapping
    public String getName(){
        return "Hello World!";
    }

    @GetMapping(path = "/student-name")
    public List getStudentName(){
        return studentService.getStudentName();
    }

    @GetMapping(path = "/api/student-list")
    public List getStudentList(){
        return studentService.getStudentRecord();
    }


    @PostMapping(path = "/api/register-student")
    public void registerStudent(@RequestBody Student student){
        studentService.addNewStudent(student);
    }

    @DeleteMapping(path = "/api/remove-student/{studentId}")
    public void deleteStudent(@PathVariable("studentId") int id){
        studentService.removeStudent(id);
    }

    @PutMapping(path = "/api/update-student/{studentId}")
    public void updateStudent(@PathVariable("studentId") int id,
                              @RequestParam String name,
                              @RequestParam String dob){
      //  LocalDate dobirth = LocalDate.
        studentService.updateStudent(id, name, LocalDate.parse(dob));
    }
}
